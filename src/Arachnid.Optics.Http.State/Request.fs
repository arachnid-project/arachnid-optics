﻿namespace Arachnid.Optics.Http.Cors

open Aether.Operators
open Arachnid.Core
open Arachnid.Optics
open Arachnid.Optics.Http
open Arachnid.Types.Http.State

// Request

/// Optics for working with individual elements of the request as part of the
/// Arachnid state, including access to typed data using the Arachnid Types
/// libraries where implemented.

[<RequireQualifiedAccess>]
module Request =

    /// Optics for request headers, usually given as lenses from
    /// State -> 'a option, where 'a is a strongly typed representation of an
    /// optional header. Headers may be added and removed by using the common
    /// Arachnid optic functionality with Option values.

    [<RequireQualifiedAccess>]
    module Headers =

        let private value_ key (tryParse, format) =
                Request.header_ key
            >-> Option.mapEpimorphism (tryParse >> Option.ofResult, format)

        /// A lens from State -> Cookie option, accessing
        /// the optional Cookie header.

        let cookie_ =
            value_ "Cookie" (Cookie.tryParse, Cookie.format)

