﻿namespace Arachnid.Optics.Http.Patch

open Aether.Operators
open Arachnid.Core
open Arachnid.Optics
open Arachnid.Optics.Http
open Arachnid.Types.Http.Patch

// Response

/// Optics for working with individual elements of the response as part of the
/// Arachnid state, including access to typed data using the Arachnid Types libraries
/// where implemented.

[<RequireQualifiedAccess>]
module Response =

    /// Optics for response headers, usually given as lenses from
    /// State -> 'a option, where 'a is a strongly typed representation of an
    /// optional header. Headers may be added and removed by using the common
    /// Arachnid optic functionality with Option values.

    [<RequireQualifiedAccess>]
    module Headers =

        let private value_ key (tryParse, format) =
                Response.header_ key
            >-> Option.mapEpimorphism (tryParse >> Option.ofResult, format)

        /// A lens from State -> AcceptPatch option, accessing the optional
        /// Accept-Patch header.

        let acceptPatch_ =
            value_ "Accept-Patch" (AcceptPatch.tryParse, AcceptPatch.format)
