# Arachnid Optics

## Overview

Arachnid Optics provides custom optics from the loosely-typed OWIN data structures to the strongly typed [Arachnid Types](https://gitlab.com/arachnid-project/arachnid-types) data structures, enabling a consistent and safe approach to working with request and response data.

## Status

[![pipeline status](https://gitlab.com/arachnid-project/arachnid-optics/badges/master/pipeline.svg)](https://gitlab.com/arachnid-project/arachnid-optics/commits/master)

## See Also

For more information see the [meta-repository for the Arachnid Web Stack](https://gitlab.com/arachnid-project/arachnid), along with the main [arachnid.io](https://arachnid.io) site.
